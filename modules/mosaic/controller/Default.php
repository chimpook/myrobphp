<?php

require_once ROOT_PATH.MODULES_PATH."/mosaic/model/Default.php";
require_once ROOT_PATH.MODULES_PATH."/mosaic/view/Default.php";

class MosaicController
{
    protected $model;
    protected $view;

    public function __construct($var, $prm)
    {
        $this->model = new MosaicModel($var, $prm);
    }

    public function output()
    {
        $data = $this->model->getData();
        $prm = $this->model->getPrm();
        $debug = $this->model->getDebug();
        $this->view = new MosaicView($data, $prm, $debug);
        return $this->view->output();
    }
}

class_alias('MosaicController', 'Mosaic');
