add_load_event( function() {

    mosaic.process();

});

var mosaic = {

    color : "blue", // colorname, rgba(x, x, x, a), some, random

    opacity : 1, // number, some, random

    opacity_min : 0.4,

    opacity_max : 1,

    shadow : "0 0 4px orange", // css box-shadow

    effect : "static", // static, switching, looping, blink

    dots : ".mosaic .letter table td:not(:empty)",

    signs : [],

    mult : 0.05,

    time : 100,

    process: function() {

        if ( $( ".mosaic input[name='color']" ) ) {
            mosaic.color = $( ".mosaic input[name='color']" ).val();
        }
        if ( $( ".mosaic input[name='opacity']" ) ) {
            mosaic.opacity = $( ".mosaic input[name='opacity']" ).val();
        }
        if ( $( ".mosaic input[name='opacity_min']" ) ) {
            mosaic.opacity_min = parseFloat(
                    $( ".mosaic input[name='opacity_min']" ).val()
                );
        }
        if ( $( ".mosaic input[name='opacity_max']" ) ) {
            mosaic.opacity_max = parseFloat(
                    $( ".mosaic input[name='opacity_max']" ).val()
                );
        }
        if ( $( ".mosaic input[name='shadow']" ) ) {
            mosaic.shadow = $( ".mosaic input[name='shadow']" ).val();
        }
        if ( $( ".mosaic input[name='effect']" ) ) {
            mosaic.effect = $( ".mosaic input[name='effect']" ).val();
        }

        mosaic.colorize();

        if (mosaic.effect === 'switching') {
            var IntervalID = setInterval( function() {
                mosaic.colorize();
            }, mosaic.time);
        } else if (mosaic.effect === 'looping') {
            var IntervalID = setInterval( function() {
                mosaic.looping();
            }, mosaic.time);
        } else if (mosaic.effect === 'blinking') {
            $( mosaic.dots ).each( function( index, element ) {
                mosaic.signs[index] = 1;
            });
            var IntervalID = setInterval( function() {
                mosaic.blinking();
            }, mosaic.time);
        }
    },

    blinking : function() {
        var a = null;

        $( mosaic.dots ).each( function( index, element ) {
            a = $( element ).css("opacity");
            a = a - mosaic.mult * mosaic.signs[index];
            if ( a < mosaic.opacity_min ){
                a = mosaic.opacity_min;
                mosaic.signs[index] = -1;
            } else if (a > mosaic.opacity_max) {
                a = mosaic.opacity_max;
                mosaic.signs[index] = 1;
            }
            $( element ).css("opacity", a);
        });
    },

    looping : function() {

        var a = null;

        $( mosaic.dots ).each( function( index, element ) {
            a = $( element ).css("opacity");
            a = a - mosaic.mult;
            if (a < 0) {
                a = 1;
            }
            $( element ).css("opacity", a);
        });

    },

    colorize : function() {

        var r = null;
        var g = null;
        var b = null;
        var a = null;
        var color = null;
       
       // set color 
        if (mosaic.color === 'some') {

            r = Math.floor(Math.random() * 255);
            g = Math.floor(Math.random() * 255);
            b = Math.floor(Math.random() * 255);
            color = "rgb("+r+", "+g+", "+b+" )";
            $( mosaic.dots ).css( "background", color );

        } else if (mosaic.color === 'random') {

            $( mosaic.dots ).each( function( index, element ) {
                r = Math.floor(Math.random() * 255);
                g = Math.floor(Math.random() * 255);
                b = Math.floor(Math.random() * 255);
                color = "rgb("+r+", "+g+", "+b+" )";
                $( element ).css("background", color);
            });

        } else {
            $( mosaic.dots ).css( "background", mosaic.color );
        }

        // set opacity
        if (mosaic.opacity === 'some') {
            a = Math.random() * (mosaic.opacity_max - mosaic.opacity_min) 
                + mosaic.opacity_min;
            $( mosaic.dots ).css( "opacity", a );
        } else if (mosaic.opacity === 'random') {

            var r = null;
            var m0 = mosaic.opacity_min;
            var m1 = mosaic.opacity_max;
            $( mosaic.dots ).each( function( index, element ) {
                r =Math.random();
                a = r * (m1 - m0) + m0;
                $( element ).css("opacity", a);
            });
        } else {
            $( mosaic.dots ).css("opacity", mosaic.opacity);
        }

        // set shadow
        $( mosaic.dots ).css("box-shadow", mosaic.shadow);
    }
                
}
