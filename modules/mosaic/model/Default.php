<?php

class MosaicModel
{
    protected $data;
    protected $prm;
    protected $var;
    protected $debug;
    protected $dot = array('in'=>'X', 'out'=>' ');
    protected $space = array('in'=>'.', 'out'=>null);
    protected $alphabet;
    protected $alphabet_raw = <<<ALPHABET
.....
.....
.....
.....
.....

~     !     @     #     $     %     ^     &     *     (     )     _     +     |    
..... ..X.. .XXX. .X.X. .XXX. XX..X ..... .XX.. X.X.X ...X. .X... ..... ..... ..X..
.X... ..X.. X...X XXXXX X.X.. XX.X. ..X.. XX.X. .XXX. ..X.. ..X.. ..... ..X.. ..X..
X.X.X ..X.. X.XXX .X.X. XXXX. ..X.. .X.X. XXX.X XXXXX .X... ...X. ..... XXXXX ..X..
...X. ..... X.XXX XXXXX ..X.X .X.XX X...X X.XX. .XXX. ..X.. ..X.. ..... ..X.. ..X..
..... ..X.. .XXX. .X.X. XXXX. X..XX ..... .XX.X X.X.X ...X. .X... XXXXX ..... ..X..

`     '     "     :     ;     [     ]     {     }     -     /     ?     \     <     >    
X.... ..X.. .X.X. ..... ..... .XXX. .XXX. .XXX. .XXX. ..... ....X .XXX. X.... ...X. .X...
.X... ..X.. .X.X. ..XX. ..XX. .X... ...X. .X... ...X. ..... ...X. X...X .X... ..X.. ..X..
..... ..... ..... ..... ..... .X... ...X. X.... ....X .XXX. ..X.. ..XX. ..X.. .X... ...X.
..... ..... ..... ..XX. ..XX. .X... ...X. .X... ...X. ..... .X... ..... ...X. ..X.. ..X..
..... ..... ..... ..... ..X.. .XXX. .XXX. .XXX. .XXX. ..... X.... ..X.. ....X ...X. .X...

0     1     2     3     4     5     6     7     8     9    
.XXX. ....X .XXX. .XXX. X...X XXXXX .XXX. XXXXX .XXX. .XXX.
X..XX ...XX X...X X...X X...X X.... X.... ...X. X...X X...X
X.X.X ..X.X ...X. ..XX. XXXXX XXXX. XXXX. ..X.. .XXX. .XXXX
XX..X ....X .X... X...X ....X ....X X...X .X... X...X ....X
.XXX. ....X XXXXX .XXX. ....X XXXX. .XXX. X.... .XXX. .XXX.

A     B     C     D     E     F     G     H     I     J     K     L     M     N     O     P     Q     R     S     T     U     V     W     X     Y     Z    
..XXX XXXX. .XXX. XXXX. XXXXX XXXXX XXXXX X...X .XXX. XXXXX X...X X.... X...X X...X .XXX. XXXX. .XXX. XXXX. .XXXX XXXXX X...X X...X X...X X...X X...X XXXXX
.X..X X...X X...X .X..X X.... X.... X.... X...X ..X.. ....X X..X. X.... XX.XX XX..X X...X X...X X...X X...X X.... ..X.. X...X X...X X...X .X.X. X...X ....X
X...X XXXX. X.... .X..X XXX.. XXX.. X..XX XXXXX ..X.. ....X XXX.. X.... X.X.X X.X.X X...X XXXX. X.X.X XXXX. .XXX. ..X.. X...X .X.X. X.X.X ..X.. .XXX. ..XX.
XXXXX X...X X...X .X..X X.... X.... X...X X...X ..X.. X...X X..X. X...X X...X X..XX X...X X.... X..X. X...X ....X ..X.. X...X .X.X. X.X.X .X.X. ..X.. .XX..
X...X XXXX. .XXX. XXXX. XXXXX X.... XXXXX X...X .XXX. .XXX. X...X XXXXX X...X X...X .XXX. X.... .XX.X X...X XXXX. ..X.. .XXX. ..X.. .X.X. X...X ..X.. XXXXX
ALPHABET;

    public function __construct($var, $prm)
    {
        $this->debug = null;
        $this->var = strtoupper(htmlspecialchars($var));
        $this->prm = $prm;
        $this->retrieveAlphabet();
        $this->retrieveData();
    }

    protected function retrieveAlphabet()
    {
        $alphabet_blocks = explode("\n\n", $this->alphabet_raw);
        $dx = $dy = 0;
        foreach($alphabet_blocks as $index=>$block)
        {
            if ($index == 0) {
                $dx = strpos($block, "\n");
                $dy = (strlen($block) + 1) / ($dx + 1);
                $matrix = '<table>';
                for($y=0; $y<$dy; $y++) {
                    $matrix .= '<tr>';
                    for ($x=0; $x<$dx; $x++) {
                        $matrix .= '<td>';
                        $dd = $y + 1 + $x;
                        if ($block[$dd] === $this->dot['in']) {
                            $matrix .= $this->dot['out'];
                        } else {
                            $matrix .= $this->space['out'];
                        }
                    }
                    $matrix .= '</tr>';
                }
                $matrix .= '</table>';
                
                $this->alphabet['settings']['dx'] = $dx; // Размер матрицы по OX
                $this->alphabet['settings']['dy'] = $dy; // Размер матрицы по OY
                $this->alphabet['data'][' '] = $matrix;
            } else {
                if($dx == 0 || $dy == 0) {
                    die("Broken alphabet array");
                }
                $bl = strpos($block, "\n"); // Длина блока
                $bs = ($bl + 1) / ($dx + 1); // Размер блока
                for ($i=0; $i<$bs; $i++){
                    $d0 = $i * ($dx+1);
                    $symbol = $block[$d0];
                    $matrix = '<table>';
                    for ($y=0; $y<$dy; $y++){
                        $matrix .= '<tr>';
                        for ($x=0; $x<$dx; $x++){
                            $matrix .= '<td>';
                            $dd = $d0 + ($bl + 1) * ($y + 1) + $x;
                            if ($block[$dd] === $this->dot['in']) {
                                $matrix .= $this->dot['out'];
                            } else {
                                $matrix .= $this->space['out'];
                            }
                            $matrix .= '</td>';
                        }
                        $matrix .= '</tr>';
                    }
                    $matrix .= '</table>';
                    $this->alphabet['data'][$symbol] = $matrix;
                }
            }
        }
        foreach ($this->alphabet['data'] as $symbol=>$string) {
            $this->debug .= '[' . $symbol . '] ';
        }
        $this->debug .= '<br/>' . $this->alphabet['data']['T'];
    }

    protected function retrieveData()
    {
        $this->data = array();
        $letters = str_split($this->var);
        foreach ($letters as $letter){
            if (isset($this->alphabet['data'][$letter])) {
                $this->data[] = $this->alphabet['data'][$letter];
            } else {
                $this->data[] = $this->alphabet['data'][' '];
            }
        }
    }

    public function getPrm()
    {
        return $this->prm;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getDebug()
    {
        return $this->debug;
    }

}
