<?php

class MosaicView
{
    protected $data;
    protected $prm;
    protected $debug;
    protected $style;
    protected $csslist;
    protected $jslist;
    protected $content;

    public function __construct($data, $prm, $debug)
    {
        $this->data = $data;
        $this->prm = $prm;
        if (!isset($this->prm['tile_width'])) {
            $this->prm['tile_width'] = '2px';
        }
        if (!isset($this->prm['tile_height'])) {
            $this->prm['tile_height'] = '2px';
        }
        if (!isset($this->prm['opacity_min'])) {
            $this->prm['opacity_min'] = '0.4';
        }
        if (!isset($this->prm['opacity_max'])) {
            $this->prm['opacity_max'] = '1';
        }
        $this->debug = $debug;
        $this->style = null;
        $this->csslist[] = MODULES_PATH. '/mosaic/view/css/Default.css';
        $this->jslist[] = MODULES_PATH. '/mosaic/controller/js/Default.js';
        $this->make();
    }

    protected function make()
    {
        $this->style = '.mosaic .letter table td { '
            . 'width: ' . $this->prm['tile_width'] . '; '
            . 'height: ' . $this->prm['tile_height'] . '; }';
        $this->content	= '<div class="mosaic">';
        if (isset($this->prm['color'])) {
            $this->content .= '<input name="color" type="hidden" value="'.$this->prm['color'].'" />';
        }
        if (isset($this->prm['opacity'])) {
            $this->content .= '<input name="opacity" type="hidden" value="'.$this->prm['opacity'].'" />';
        }
        if (isset($this->prm['opacity_min'])) {
            $this->content .= '<input name="opacity_min" type="hidden" value="'.$this->prm['opacity_min'].'" />';
        }
        if (isset($this->prm['opacity_max'])) {
            $this->content .= '<input name="opacity_max" type="hidden" value="'.$this->prm['opacity_max'].'" />';
        }
        if (isset($this->prm['shadow'])) {
            $this->content .= '<input name="shadow" type="hidden" value="'.$this->prm['shadow'].'" />';
        }
        if (isset($this->prm['effect'])) {
            $this->content .= '<input name="effect" type="hidden" value="'.$this->prm['effect'].'" />';
        }
        foreach ($this->data as $letter){
            $this->content .= '<div class="letter">' . $letter . '</div>';
        }
        $this->content .= '</div>';
    }

    public function output()
    {
        return array( "content"=>$this->content, "style"=>$this->style, "csslist"=>$this->csslist, "jslist"=>$this->jslist, "debug"=>$this->debug );
    }
}
