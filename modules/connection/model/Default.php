<?php

class ConnectionModel
{
    protected $test;
    protected $link;
    protected $parm;

    public function __construct($parm)
    {
        $this->parm = $parm;
    }

    public function open()
    {
        if ( ! isset($this->link)) {
            $this->link 
                = new mysqli(
                    $this->parm['server'], 
                    $this->parm['user'], 
                    $this->parm['password'], 
                    $this->parm['database']
                ) or die($this->link->connect_error);
            $this->link->query(
                "SET NAMES 'utf8'"
            ) or die("@".__FILE__."@".__LINE__." : ".$this->link->error());
        }
    }

    public function close()
    {
        if (isset($this->link)) {
            $this->link->close();
            unset($this->link);
        }
    }

    public function getParm()
    {
        return $this->parm;
    }

    public function getLink()
    {
        return $this->link;
    }
}
