<?php

class ConnectionView
{
    protected $csslist;
    protected $content;

    public function __construct($parm, $debug)
    {
        $this->csslist[] = MODULES_PATH.'/connection/view/css/Default.css';
        $this->content 
            = '<div class="msg info">'
            . '<table>'
            . '<tr>'
            . '<th>Сервер</th>'
            . '<td>' . $parm['server'] . '</td>'
            . '</tr>'
            . '<tr>'
            . '<th>База данных</th>'
            . '<td>' . $parm['database'] . '</td>'
            . '</tr>'
            . '<tr>'
            . '<th>Пользователь</th>'
            . '<td>' . $parm['user'] . '</td>'
            . '</tr>'
            . '<tr>'
            . '<th>Пароль</th>'
            . '<td>' . substr($parm['password'], 0, 3) . '...</td>'
            . '</tr>'
            . '</table>'
            . '</div>';
    }

    public function output()
    {
        return array('content'=>$this->content, 'csslist'=>$this->csslist);
    }
}
