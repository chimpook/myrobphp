<?php

require_once ROOT_PATH.MODULES_PATH."/connection/model/Default.php";
require_once ROOT_PATH.MODULES_PATH."/connection/view/Default.php";

class ConnectionController
{
    protected $model;
    protected $view;

    public function __construct()
    {
        global $parameters;
        $this->model = new ConnectionModel($parameters['connection']);
    }

    public function open()
    {
        $this->model->open();
    }

    public function close()
    {
        $this->model->close();
    }

    public function getParm()
    {
        return $this->model->getParm();
    }

    public function getLink()
    {
        return $this->model->getLink();
    }

    public function output()
    {
        if ( ! isset($this->view)) {
            $parm = $this->model->getParm();
            $this->view = new ConnectionView($parm);
        }
        return $this->view->output();
    }
}

class_alias('ConnectionController', 'Connection');
