<?php

class PanelModel
{
    protected $link;
    protected $uid;
    protected $tbset;
    protected $prm;
    protected $debug;

    public function __construction($link, $uid)
    {
        $this->link = $link;
        $this->uid = $uid;

        global $tables;

        $this->tbset = $tables['tbset']['name'];
        $this->debug = '<h2>' . __FILE . '</h2>';
    }

    public function retrievePrm()
    {
    }

    public function getPrm()
    {
        return $this->prm;
    }

    public function getDebug()
    {
        return $this->debug;
    }
}
