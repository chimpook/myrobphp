<?php

class PanelView
{
    protected $csslist;
    protected $jslist;
    protected $prm;
    protected $debug;
    protected $content;

    public function __construct($prm, $debug)
    {
        $this->prm = $prm;
        $this->debug = $debug;
        $this->csslist[] = MODULES_PATH . '/panel/view/css/Default.css';
        $this->jslist[] = MODULES_PATH . '/panel/controller/js/Default.js';
        $this->content = <<< PANEL
<div class="panel">
    <div class="leftside">
        <button class="menu btn btn-default" type="button">
            <span class="glyphicon glyphicon-th"></span>
        </button>
        <div class="label">myrobPHP</div>
        <div class="search">
            <div class="input-group input-group-lg">
                <input name="search" type="text" class="form-control" placeholder="Search for ..." value="" />
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
        </div>
    </div>
    <div class="rightside">
        <button name="logout" class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-log-out"></span>
        </button>
        <button name="sort" class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-sort"></span>
        </button>
        <button name="view" class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-list"></span>
        </button>
    </div>
    <button name="add" class="btn btn-default" type="button">
        <span class="glyphicon glyphicon-plus"></span>
    </button>
</div>
PANEL;
    }

    public function output()
    {
        return array('content'=>$this->content, 'csslist'=>$this->csslist, 'jslist'=>$this->jslist);
    }
}
