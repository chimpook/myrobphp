<?php

require_once ROOT_PATH . MODULES_PATH . "/panel/model/Default.php";
require_once ROOT_PATH . MODULES_PATH . "/panel/view/Default.php";

class PanelController
{
    protected $model;
    protected $view;

    public function __construct($link, $uid)
    {
        $this->model = new PanelModel($link, $uid);
        $this->model->retrievePrm();
        $prm = $this->model->getPrm();
        $debug = $this->model->getDebug();
        $this->view = new PanelView($prm, $debug);
    }

    public function output()
    {
        return $this->view->output();
    }
}

class_alias('PanelController', 'Panel');
