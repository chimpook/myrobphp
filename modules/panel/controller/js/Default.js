add_load_event( function() {
    $( "button[name=logout]" ).on ('click', function() {
        panel.logout();
    });
});

panel = {

    logout : function() {

        $( ".debug" ).load(
                "/modules/panel/controller/ajax/logout.php",
                function() {
                    location.reload();
                });

    }

};
