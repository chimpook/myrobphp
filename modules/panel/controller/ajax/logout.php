<?php

ini_set('display_errors', 1);

require_once realpath($_SERVER["DOCUMENT_ROOT"]).'/etc/globals.php';

useModule("connection");
useModule("session");

$connection = new Connection();
$connection->open();
$link = $connection->getLink();

$session = new Session($link);
$session->update();
$session->close();
