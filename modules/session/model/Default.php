<?php

class SessionModel
{
    protected $tables;
    protected $link;
    protected $sid;
    protected $data;

    public function __construct($link)
    {
        global $tables;
        $this->tables = $tables;
        $this->link = $link;
        session_start();
        $this->sid = session_id();
    }

    public function init($account, $acl)
    {
        $tbses = $this->tables['tbses']['name'];

        // Идентификатор сессии должен быть уникальным
        $is_checked = false;
        while ( ! $is_checked) { 
            $result_ses = $this->link->query(
                "SELECT * FROM `$tbses` WHERE `sid`='$this->sid'"
            ) or die("@".__FILE__."@".__LINE__." : ".$this->link->error);
            $number_ses = $result_ses->num_rows;
            if ($number_ses) {
                session_regenerate_id();
                $this->sid = session_id();
            } else {
                $is_checked = true;
            }
        }

        $this->data = array(
            "uid" => $account['uid'],
            "name" => $account['name'],
            "acl" => $account['acl']
        );
        $_SESSION['data'] = $this->data;
        $this->update();
    }

    public function hasDataSession()
    {
        if (isset($_SESSION['data'])) {
            $this->data = $_SESSION['data'];
            return true;
        } else {
            return false;
        }		
    }

    public function getField($fname)
    {
        return $this->data[$fname];
    }

    public function getData()
    {
        return $this->data;
    }

    public function getSid()
    {
        return $this->sid;
    }

    public function update()
    {
        $tbses = $this->tables['tbses']['name'];

        // Поиск записи сессии
        $result_ses = $this->link->query(
            "SELECT * FROM `$tbses` WHERE `sid`='$this->sid'"
        ) or die("@".__FILE__."@".__LINE__." : ".$this->link->error);
        $number_ses = $result_ses->num_rows;
        if ( ! $number_ses) {
            // Если такой записи в таблице нет - создаем ее
            $this->link->query(
                "INSERT INTO `$tbses` (`id`, `uid`, `sid`, `updtime`) "
                . "VALUES ('', '".$this->data['uid']."', '$this->sid', NOW())"
            ) or die("@" . __FILE__ . "@" . __LINE__ . " : " . $this->link->error);
        } else {
            // Если сессия зарегистрирована - обновляем время доступа
            $this->link->query(
                "UPDATE `" . $tbses . "` SET `updtime`=NOW() WHERE `sid`='"
                . $this->sid . "'"
            ) or die("@" . __FILE__ . "@" . __LINE__ . " : " . $this->link->error);
        }

        // Удаление устаревших сессий (не обновлявшихся более 20 минут)
        $this->link->query(
            "DELETE FROM `" . $tbses 
            . "` WHERE `updtime` < NOW() - INTERVAL '20' MINUTE"
        ) or die("@" . __FILE__ . "@" . __LINE__ . " : " . $this->link->error);
    }

    public function close()
    {
        $tbses = $this->tables['tbses']['name'];
        $uid = $this->tables['tbusr']['linked_id'];
        $this->link->query(
            "DELETE FROM `" . $tbses . "` WHERE `sid`='" . $this->sid . "'" 
        ) or die("@" . __FILE__ . "@" . __LINE__ . " : " . $this->link->error);
        session_unset();
        session_destroy();
    }
}
