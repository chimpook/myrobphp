<?php

require_once ROOT_PATH . MODULES_PATH . "/session/model/Default.php";
require_once ROOT_PATH . MODULES_PATH . "/session/view/Default.php";

class SessionController
{
    protected $model;
    protected $view;

    public function __construct($link)
    {
        $this->model = new SessionModel($link);
    }

    public function init($account, $acl)
    {
        $this->model->init($account, $acl);
    }

    public function check()
    {
        return $this->model->hasDataSession();
    }

    public function update()
    {
        $this->model->update();
    }

    public function getField($fname)
    {
        return $this->model->getField($fname);
    }

    public function output()
    {
        if ( ! isset($this->view)) {
            $data = $this->model->getData();
            $this->view = new SessionView($data);
        }
        return $this->view->output();
    }

    public function close()
    {
        $this->model->close();
    }
}

class_alias('SessionController', 'Session');
