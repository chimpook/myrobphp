<?php

class SessionView
{
    protected $data;
    protected $csslist;
    protected $view;

    public function __construct($data)
    {
        $this->data = $data;
        $this->csslist[] = MODULES_PATH . '/session/view/css/Default.css';
        $this->make();
    }

    protected function make()
    {
        $this->view 
            = '<div class="msg info">'
            . '<h2>Информация о сессии пользователя</h2><hr /><table>';
        foreach ($this->data as $fname=>$fdata) {
            $this->view .= '<tr><th>' . $fname . '</th><td>' . $fdata . '</td></tr>';
        }
        $this->view .= '</table></div>';
    }

    public function output()
    {
        return array("content"=>$this->view, "csslist"=>$this->csslist);
    }
}
