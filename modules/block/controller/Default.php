<?php

require_once(ROOT_PATH . MODULES_PATH . "/block/model/Default.php");
require_once(ROOT_PATH . MODULES_PATH . "/block/view/Default.php");

class BlockController
{
	protected $model;
	protected $view;
    protected $hideable;    // Если параметр задан - блок не выводится при отсутствии содежимого. 
                            // Например, блок .debug
	
	public function __construct($identifier, $hideable=false)
	{
		$this->model = new BlockModel($identifier);
		$this->hideable = $hideable;
	}
		
    public function append($object)
    {
		if (isset($object["style"])) {
			$this->model->addStyle($object["style"]);
		}
		if (isset($object["script"])) {
			$this->model->addScript($object["script"]);
		}
		if (isset($object["content"])) {
			$this->model->addContent($object["content"]);
		}
        if (isset($object["debug"])) {
            $this->model->addDebug($object["debug"]);
        }
		if (isset($object["csslist"])) {
			$this->model->addCSSlist($object["csslist"]);
		}
		if (isset($object["jslist"])) {
			$this->model->addJSlist($object["jslist"]);
		}
	}

    public function addContent($content)
    {
        $this->model->addContent($content);
    }

    public function addDebug($debug)
    {
        $this->model->addDebug($debug);
    }

    public function addStyle($style)
    {
        $this->model->addStyle($style);
    }

    public function addScript($script)
    {
        $this->model->addScript($script);
    }

	public function addCSS($css)
	{
		$this->model->addCSS($css);
	}
    
    public function addJS($js)
    {
		$this->model->addJS($js);
	}
    
	public function output()
	{
		if ($this->hideable && !$this->model->getContent() && !$this->model->getDebug()) {
			return null;
		}
		
		if (!isset($this->view)) {
			$identifier = $this->model->getIdentifier();
			$style = $this->model->getStyle();
			$script = $this->model->getScript();
			$content = $this->model->getContent();
			$debug = $this->model->getDebug();
			$csslist = $this->model->getCSSlist();
			$jslist = $this->model->getJSlist();
			$this->view = new BlockView($identifier, $style, $script, $content, $debug, $csslist, $jslist);
		}
		return $this->view->output();
	}
}

class_alias('BlockController', 'Block');
