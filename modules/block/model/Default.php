<?php

class BlockModel
{
	protected $identifier;
	protected $style;
	protected $script;
	protected $content;
    protected $debug;
	protected $csslist;
	protected $jslist;
	
	public function __construct($identifier=null)
	{
		$this->identifier = $identifier;
		$this->style = $this->script = $this->content = null;
		$this->csslist = $this->jslist = array();
	}
	
	public function addStyle($style)
	{
		$this->style .= $style;
	}
	
	public function addScript($script)
	{
		$this->script .= $script;
	}

	public function addContent($content)
	{
		$this->content .= $content;
	}
	
	public function addDebug($debug)
	{
		$this->debug .= $debug;
	}
	
	public function addCSS($css)
	{
		$this->csslist[] = $css;
	}

	public function addCSSlist($csslist)
	{
		foreach ($csslist as $css)
		{
			$this->addCSS($css);
		}
	}
	
	public function addJS($js)
	{
		$this->jslist[] = $js;
	}
	
	public function addJSlist($jslist)
	{
		foreach ($jslist as $js)
		{
			$this->addJS($js);
		}
	}
	
	public function getIdentifier()
	{
		return $this->identifier;
	}
	
	public function getStyle()
	{
		return $this->style;
	}
	
	public function getScript()
	{
		return $this->script;
	}
	
	public function getContent()
	{
		return $this->content;
	}

    public function getDebug()
    {
        return $this->debug;
    }
	
	public function getCSSlist()
	{
		return $this->csslist;
	}
	
	public function getJSlist()
	{
		return $this->jslist;
	}
}
