<?php

class BlockView
{
	protected $identifier;
	protected $style;
	protected $script;
	protected $content;
    protected $debug;
	protected $csslist;
	protected $jslist;
	
	public function __construct($identifier, $style, $script, $content, $debug, $csslist, $jslist)
	{
		$this->identifier = $identifier;
		$this->style = $style;
		$this->script = $script;
        $this->content = '<div '. $identifier . '>' . $content . '</div>';
        $this->debug = $debug;
		$this->csslist = $csslist;
		$this->jslist = $jslist;
	}
	
	public function output()
	{
		return array(
			'style'=>$this->style, 
			'script'=>$this->script, 
			'content'=>$this->content, 
            'debug' => $this->debug,
			'csslist'=>$this->csslist,
			'jslist'=>$this->jslist );
	}
}
