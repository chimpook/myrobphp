<?php

require_once ROOT_PATH.MODULES_PATH."/login/model/Default.php";
require_once ROOT_PATH.MODULES_PATH."/login/view/Default.php";

class LoginController
{
    protected $model;
    protected $view;

    public function __construct($link)
    {
        $this->model = new LoginModel($link);
        $this->model->check();
    }

    public function isAccessGranted()
    {
        return $this->model->isAccessGranted();
    }

    public function getAccount()
    {
        return $this->model->getAccount();
    }

    public function output()
    {
        if ( ! isset($this->view)) {
            $message = $this->model->getMessage();
            $debug = $this->model->getDebug();
            $this->view = new LoginView($message, $debug);
        }
        return $this->view->output();
    }
}

class_alias('LoginController', 'Login');
