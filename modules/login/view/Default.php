<?php

class LoginView
{
    protected $message;
    protected $style;
    protected $csslist;
    protected $jslist;
    protected $content;
    protected $debug;

    public function __construct($message, $debug)
    {
        $this->message = $message;
        $this->debug = $debug;
        $this->style = null;
        $this->csslist[] = MODULES_PATH. '/login/view/css/Default.css';
        $this->jslist[] = MODULES_PATH. '/login/controller/js/Default.js';
        $this->make();
    }

    protected function make()
    {
        $version = VERSION;
        $disabled = null;
        if (BLOCKMODE) {
            $disabled = 'disabled="disabled"';
        }
        useModule("mosaic");

        $mosaic = new Mosaic(
            "EAST-SITE", 
            array(
                'color'=>'darkseagreen', 
                'opacity'=>'random',
                'opacity_min'=>'0.4',
                'opacity_max'=>'1',
                'shadow'=>'0 0 2px darkslategray', 
                'effect'=>'blinking',
                'tile_width'=>'8px',
                'tile_height'=>'8px'
            )
        );

/*
        $mosaic = new Mosaic(
            "KONTINENT-TAU", 
            array(
                'color'=>'darkseagreen', 
                'opacity'=>'random',
                'opacity_min'=>'0.4',
                'opacity_max'=>'1',
                'shadow'=>'0 0 2px darkslategray', 
                'effect'=>'blinking',
                'tile_width'=>'5px',
                'tile_height'=>'8px'
            )
        );
*/
        $this->csslist[] = $mosaic->output()['csslist'][0];
        $this->jslist[] = $mosaic->output()['jslist'][0];
        $this->style .= $mosaic->output()['style'];
        $letters = $mosaic->output()['content'];
        //$title = 'EAST-SITE';
        $title = null;
        
        $this->content = <<<LOGIN_HEADER_MSG
<div class="container">
    <div class="login">
        <div class="logo">
            <div class="letters">$letters</div>
            <div class="title">
            <h2>$title</h2>
            </div>
        </div>
        <div class="name">My register of belongings. Ver. <span class="version">$version</span></div>
        <div class="panel">
            <form action="" method="POST">
                <div class="input-group">
                <input name="username" type="text" class="form-control" placeholder="Username" required autofocus $disabled />
                <input name="password" type="password" class="form-control" placeholder="Password" required $disabled />
                <input name="submit" type="submit" class="form-control" value="Вход" class="btn btn-lg btn-primary btn-block" $disabled />
                </div>
            </form>
LOGIN_HEADER_MSG;
        $this->content	.= '</div>';
        if ($this->message) {
            $this->content .= '<span class="alert alert-warning">'.$this->message.'&nbsp; Повторите ввод. </span>';
        }
        $this->content	.= '</div></div>';
    }

    public function output()
    {
        return array( "content"=>$this->content, "style"=>$this->style, "csslist"=>$this->csslist, "jslist"=>$this->jslist, "debug"=>$this->debug );
    }
}
