<?php

class LoginModel
{
    protected $tables;
    protected $link;
    protected $access;
    protected $message;
    protected $account;
    protected $acl;
    protected $debug;

    public function __construct($link)
    {
        global $tables;
        $this->tables = $tables;
        $this->link = $link;
        $this->access = false;
        $this->debug = null;
    }

    protected function retrieveCredentials()
    {
        list($username, $password) = null;
        if (isset($_POST['username'])) {
            $username = trim(htmlspecialchars(stripcslashes($_POST['username'])));
        }
        if (isset($_POST['password'])) {
            $password = trim(htmlspecialchars(stripcslashes($_POST['password'])));
        }
        return array($username, $password);
    }

    public function check()
    {
        $tbusr = $this->tables['tbusr']['name'];

        list($username, $password) = $this->retrieveCredentials();

        if (empty($username)) {
            $this->access = false;
            $this->message = null;
            return;
        }

        $result_usr = $this->link->query(
            "SELECT * FROM `$tbusr` WHERE `name`='$username'"
        ) or die("@".__FILE__."@".__LINE__." : ".$this->link->error());

        $number_usr = $result_usr->num_rows;

        if ( ! $number_usr) {
            $this->access = false;
            $this->message = 'Пользователь с таким именем не зарегистрирован.';
            return;
        }

        $row_usr = $result_usr->fetch_assoc();

        if ( strcmp($password, $row_usr['password'])) {
            $this->access = false;
            $this->message = 'Указан неверный пароль.';
            return;
        }

        // it is ok
        $this->access = true;
        $this->message = 'Доступ разрешен.';

        $this->account = array(
            "uid" => $row_usr['id'],
            "name" => $row_usr['name'],
            "acl" => $row_usr['acl']
        );

    }

    public function isAccessGranted()
    {
        return $this->access;   
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function getDebug()
    {
        return $this->debug;
    }
}
