<?php

class BlankModel
{
    protected $data;

    public function __construct($var)
    {
        $this->retrieveData($var);
    }

    protected function retrieveData($var)
    {
        $this->data = '<div class="container">' . $var . '</div>';
    }

    public function getData()
    {
        return $this->data;
    }

}
