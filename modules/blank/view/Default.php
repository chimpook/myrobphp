<?php

class BlankView
{
    protected $data;
    protected $csslist;
    protected $jslist;
    protected $content;

    public function __construct($data)
    {
        $this->data = $data;
        $this->csslist[] = MODULES_PATH. '/blank/view/css/Default.css';
        $this->jslist[] = MODULES_PATH. '/blank/controller/js/Default.js';
        $this->make();
    }

    protected function make()
    {
        $this->content	.= '<div class="blank">' . $this->data . '</div>';
    }

    public function output()
    {
        return array( "content"=>$this->content, "csslist"=>$this->csslist, "jslist"=>$this->jslist );
    }
}
