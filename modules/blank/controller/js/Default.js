add_load_event( function() {

    blank.colorize();

});

var blank = {

    colorize: function() {

        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        var a = Math.random() * (1 - 0.4) + 0.4;
        $( ".blank" )
            .css( 
                "background", 
                "rgba("+r+", "+g+", "+b+", "+a+" )")
            .css(
                "color",
                "rgb(" +(255-r)+", "+(255-g) +", "+(255-b)+")");
                
    }

}
