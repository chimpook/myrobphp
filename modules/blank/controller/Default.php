<?php

require_once ROOT_PATH.MODULES_PATH."/blank/model/Default.php";
require_once ROOT_PATH.MODULES_PATH."/blank/view/Default.php";

class BlankController
{
    protected $model;
    protected $view;

    public function __construct($var)
    {
        $this->model = new BlankModel($var);
    }

    public function output()
    {
        $data = $this->model->getData();
        $this->view = new BlankView($data);
        return $this->view->output();
    }
}

class_alias('BlankController', 'Blank');
