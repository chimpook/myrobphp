<?php
 
/**
 * Default "Controller" part of module "Page"
 *
 * PHP version 5
 *
 * @category  Controller
 * @package   Page
 * @author    Sergey Veselovsky <s.veselovsky@k-tau.ru>
 * @copyright 2015 Kontinent-Tau Ltd
 * @license   http://www.gnu.org/licenses/gpl.html GPL
 * @version   GIT: $Id$ 
 * @link      https://bitbucket.org/chimpook/dogovorus.git
 */

require_once ROOT_PATH . MODULES_PATH . "/page/model/Default.php";
require_once ROOT_PATH . MODULES_PATH . "/page/view/Default.php";

/**
 * "Controller" class
 *
 * @category  Controller 
 * @package   Page
 * @author    Sergey Veselovsky <s.veselovsky@k-tau.ru>
 * @license   http://www.gnu.org/licenses/gpl.html GPL
 * @link      https://bitbucket.org/chimpook/dogovorus.git
 */
class PageController
{
    protected $model;
    protected $view;

    /**
     * Controller class constructor
     *
     * @param string $title Title of page 
     *
     * @access public
     * @return void
     */
    public function __construct($title) 
    {
        $this->model = new PageModel($title);
    }

    /**
     * Append page object's definition array to page structure
     *
     * @param array $object Object's definition array 
     *
     * @access public
     * @return void
     */
    public function append($object)
    {
        if (isset($object["style"])) {
            $this->model->addStyle($object["style"]);
        }
        if (isset($object["script"])) {
            $this->model->addScript($object["script"]);
        }
        if (isset($object["content"])) {
            $this->model->addContent($object["content"]);
        }
        if (isset($object["debug"])) {
            $this->model->addDebug($object["debug"]);
        }
        if (isset($object["csslist"])) {
            $this->model->addCSSlist($object["csslist"]);
        }
        if (isset($object["jslist"])) {
            $this->model->addJSlist($object["jslist"]);
        }
    }

    /**
     * Add CSS file definition string to page's CSS summary definition array
     *
     * @param string $cssdef CSS file definition string 
     *
     * @access public
     * @return void
     */
    public function addCSS($cssdef)
    {
        $this->model->addCSS($cssdef);
    }

    /**
     * Add JS file definition string to page's JS summary definition array 
     *
     * @param string $jsdef JS file definition string
     *
     * @access public
     * @return void
     */
    public function addJS($jsdef)
    {
        $this->model->addJS($jsdef);
    }

    /**
     * Output whole page structure 
     *
     * @access public
     * @return void
     */
    public function output()
    {
        if ( ! isset($this->view)) {
            $title = $this->model->getTitle();
            $style = $this->model->getStyle();
            $script = $this->model->getScript();
            $content = $this->model->getContent();
            $debug = $this->model->getDebug();
            $csslist = $this->model->getCSSlist();
            $jslist = $this->model->getJSlist();
            $this->view 
                = new PageView($title, $style, $script, $content, $debug, $csslist, $jslist);
        }
        return $this->view->output();
    }
}

class_alias('PageController', 'Page');
