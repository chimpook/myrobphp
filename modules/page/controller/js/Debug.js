add_load_event( function() {
    $( document ).bind ("keydown", "ctrl+`", function() { debug.toggle(); });
});

var debug = {
   
    status: "hidden",

    toggle: function() {

        if ( debug.status == "hidden" ) {

            debug.unhide();

        } else {

            debug.hide();

        }

    },

    hide: function() {

        $( ".debug" )
            .removeClass( "unhidden" )
            .addClass( "hidden" );
        debug.status = "hidden";
    },

    unhide: function() {

        $( ".debug" )
            .removeClass( "hidden" )
            .addClass( "unhidden" );
        debug.status = "unhidden";
    }

}
