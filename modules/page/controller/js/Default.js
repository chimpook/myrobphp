window.onload = function() {
    //init_widgets();
    //$(".ui_draggable").draggable();
    //$(".ui_resizable").resizable();

    // Перезагрузка страницы при изменении размера окна
    $(window).bind('resize', function(e){ this.location.reload(false); });
}


/**
 * Инициализация интерфейса jQuery-UI
 */
function init_widgets() {
    $("button,input[type=submit],.s_buttons").button();
    $("select").selectmenu();
    $("input:text").button().addClass("ui-textfield");
    $(".ui_add").button({ 
        icons: { primary: "ui-icon-plus" }, 
        text: false 
    });
    $(".ui_edit").button({ 
        icons: { primary: "ui-icon-pencil" }, 
        text: false 
    });
    $(".ui_drop").button({ 
        icons: { primary: "ui-icon-close" }, 
        text: false 
    });
}

/**
 * Метод подгрузки событий инициализации
 * by Simon Willison
 * http://www.htmlgoodies.com/beyond/javascript/article.php
 * /3724571/Using-Multiple-JavaScript-Onload-Functions.htm
 */
function add_load_event(func) {
    var old_onload = window.onload;
    if (typeof(window.onload) != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (old_onload) {
                old_onload();
            }
            func();
        }
    }
}
