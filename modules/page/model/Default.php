<?php

/**
 * Default "Model" part of module "Page"
 *
 * PHP version 5
 *
 * @category  Model
 * @package   Page
 * @author    Sergey Veselovsky <s.veselovsky@k-tau.ru>
 * @copyright 2015 Kontinent-Tau Ltd
 * @license   http://www.gnu.org/licenses/gpl.html GPL
 * @version   GIT: $Id$ 
 * @link      https://bitbucket.org/chimpook/dogovorus.git
 */

/**
 * "Model" class
 *
 * @category Model 
 * @package  Page
 * @author   Sergey Veselovsky <s.veselovsky@k-tau.ru>
 * @license  http://www.gnu.org/licenses/gpl.html GPL
 * @link     https://bitbucket.org/chimpook/dogovorus.git
 */
class PageModel
{
    protected $title;
    protected $style;
    protected $script;
    protected $content;
    protected $debug;
    protected $csslist;
    protected $jslist;

    /**
     * Model class constructor
     *
     * @param string $title Title of page 
     *
     * @access public
     * @return void
     */
    public function __construct($title)
    {
        $this->title = $title;
        $this->style = $this->script = $this->content = $this->debug = null;
        $this->csslist = $this->jslist = array();
    }

    /**
     * Add style string to page's style definition summary string
     *
     * @param string $style Style string 
     *
     * @access public
     * @return void
     */
    public function addStyle($style)
    {
        $this->style .= $style;
    }

    /**
     * Add script string to page's script definition summary string
     *
     * @param string $script Script string 
     *
     * @access public
     * @return void
     */
    public function addScript($script)
    {
        $this->script .= $script;
    }

    /**
     * Add content string to page's content definition summary string
     *
     * @param string $content Content string 
     *
     * @access public
     * @return void
     */
    public function addContent($content)
    {
        $this->content .= $content;
    }

    /**
     * Add debug string to page's debug definition summary string
     *
     * @param string $debug Debug string 
     *
     * @access public
     * @return void
     */
    public function addDebug($debug)
    {
        $this->debug .= $debug;
    }

    /**
     * Add css file definition string to page's css definition array
     *
     * @param string $cssdef CSS file definition string
     *
     * @access public
     * @return void
     */
    public function addCSS($cssdef)
    {
        $this->csslist[] = $cssdef;
    }

    /**
     * Add css files definitions array to page's css definition array
     *
     * @param array $csslist CSS files definitions array
     *
     * @access public
     * @return void
     */
    public function addCSSlist($csslist)
    {
        foreach ($csslist as $cssdef) {
            $this->addCss($cssdef);
        }
    }

    /**
     * Add js file definition string to page's js definition array 
     *
     * @param string $jsdef JS file definition string
     *
     * @access public
     * @return void
     */
    public function addJS($jsdef)
    {
        $this->jslist[] = $jsdef;
    }

    /**
     * Add js files definitions array to page's js definition array
     *
     * @param array $jslist JS files definitions array
     *
     * @access public
     * @return void
     */
    public function addJSlist($jslist)
    {
        foreach ($jslist as $jsdef) {
            $this->addJS($jsdef);
        }
    }

    /**
     * Getter of title string 
     *
     * @access public
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Getter of style summary string 
     *
     * @access public
     * @return string 
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Getter of script summary string
     *
     * @access public
     * @return string 
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * Getter of content summary string 
     *
     * @access public
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Getter of debug summary string 
     *
     * @access public
     * @return string 
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * Getter of CSS summary array 
     *
     * @access public
     * @return array 
     */
    public function getCSSlist()
    {
        return $this->csslist;
    }

    /**
     * Getter of JS summary array 
     *
     * @access public
     * @return array 
     */
    public function getJSlist()
    {
        return $this->jslist;
    }
}
