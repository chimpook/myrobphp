<?php

/**
 * Default "View" part of module "Page"
 *
 * PHP version 5
 *
 * @category  View
 * @package   Page
 * @author    Sergey Veselovsky <s.veselovsky@k-tau.ru>
 * @copyright 2015 Kontinent-Tau Ltd
 * @license   http://www.gnu.org/licenses/gpl.html GPL
 * @version   GIT: $Id$ 
 * @link      https://bitbucket.org/chimpook/dogovorus.git
 */

/**
 * "View" class
 *
 * @category  View 
 * @package   Page
 * @author    Sergey Veselovsky <s.veselovsky@k-tau.ru>
 * @license   http://www.gnu.org/licenses/gpl.html GPL
 * @link      https://bitbucket.org/chimpook/dogovorus.git
 */
class PageView
{
    protected $title;
    protected $style;	// строка внутреннего блока стилей
    protected $script;	// строка внутреннего блока скриптов
    protected $content;	// строка контента
    protected $debug;   // строка отладочной информации
    protected $csslist;	// массив имен файлов css, подключаемых к странице
    protected $jslist;	// массив имен файлов js, подключаемых к странице
    protected $view;

    /**
     * View class constructor
     *
     * @param string $title   Title of page 
     * @param string $style   Style definition summary string
     * @param string $script  Script definition summary string
     * @param string $content Content defininition summary string
     * @param array  $csslist CSS files definitions summary array
     * @param array  $jslist  JS files definition summary array
     *
     * @access public
     * @return void
     */
    public function __construct($title, $style, $script, $content, $debug, $csslist, $jslist)
    {
        $this->title = $title;
        $this->style = $style;
        $this->script = $script;
        $this->content = $content;
        $this->debug = $debug;
        $this->csslist = array_merge(
            array(LIBS_PATH . '/reset/reset.css'),
            array(THEMES_PATH . '/' . THEME . '/jquery-ui.css'),
            array(LIBS_PATH . '/bootstrap/css/bootstrap.min.css'),
//            array(LIBS_PATH . '/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'),
//            array(LIBS_PATH . '/awesome-bootstrap-checkbox/bower_components/Font-Awesome/css/font-awesome.css'),
            array(MODULES_PATH . '/page/view/css/Default.css'), 
            array(MODULES_PATH . '/page/view/css/Debug.css'), 
            $csslist 
        );
        $this->jslist = array_merge(
            array(LIBS_PATH . '/jquery/2.1.1/jquery.min.js'),
//            array(LIBS_PATH . '/slimScroll/1.3.6/jquery.slimscroll.min.js'),
            array(LIBS_PATH . '/jquery.hotkeys/jquery.hotkeys.js'),
//            array(LIBS_PATH . '/autosize/dist/autosize.min.js'),
            array(THEMES_PATH . '/' . THEME.'/jquery-ui.js'),
//            array(LIBS_PATH . '/jquery-ui/ui/i18n/datepicker-ru.js'),
            array(MODULES_PATH . '/page/controller/js/Default.js'),
            array(MODULES_PATH . '/page/controller/js/Debug.js'),
            $jslist
        );
        $this->make();
    }

    /**
     * View maker 
     *
     * @access protected 
     * @return void
     */
    protected function make()
    {
        $this->view
            = '<!DOCTYPE html>'
            . '<html>'
            . '<head>'
            . '<meta charset="utf-8">'
            . '<meta http-equiv="X-UA-Compatible" content="IE=edge">'
            . '<meta name="viewport" content="width=device-width, initial-scale=1">'
            . '<meta http-equiv="content-type"; content="text/html">'
            . '<link rel="icon" href="/favicon.ico">'
            . '<title>' . $this->title . '</title>';
        foreach ($this->csslist as $cssdef) {
            $this->view
                .= '<link rel="stylesheet" type="text/css" href="' . $cssdef . '">';
        }
        foreach ($this->jslist as $jsdef) {
            $this->view 
                .= '<script type="text/javascript" src="' . $jsdef . '"></script>';
        }
        $this->view 
            .= '<style>' . $this->style . '</style>'
             . '<script>'. $this->script . '</script>'
             . '</head>'
             . '<body>' 
             . $this->content 
             . '<div class="debug hidden">'
             . $this->debug
             . '</div>'
             . '</body>'
             . '</html>';
    }

    /**
     * Output view of page
     *
     * @access public
     * @return string 
     */
    public function output()
    {
        return $this->view;
    }
}
