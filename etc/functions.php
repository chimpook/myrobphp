<?php

function useModule($module, $controller="Default.php")
{
    include_once ROOT_PATH.MODULES_PATH.'/'.$module.'/controller/'.$controller;
}

function ifset($var, $val, $noterr, $rawmode=false)
{
    if (isset($_POST[$var])){
        if($rawmode){
            return $_POST[$var];
        }
        return htmlspecialchars($_POST[$var]);
    }

    if (isset($_GET[$var])){
        if($rawmode){
            return $_GET[$var];
        }
        return htmlspecialchars($_GET[$var]);
    }

    if ($noterr){
        return $val;
    }
    die($val);
}

function loripsum($counter)
{
    $content = null;
    $loripsum = <<<LORIPSUM
<br />
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Eam tum adesse, cum dolor omnis absit; Haeret in salebra. Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam; Cupiditates non Epicuri divisione finiebat, sed sua satietate. Quis est, qui non oderit libidinosam, protervam adolescentiam? Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Primum in nostrane potestate est, quid meminerimus? Quis non odit sordidos, vanos, leves, futtiles? Non est ista, inquam, Piso, magna dissensio. Nihil sane. Quis istud possit, inquit, negare?
<br />
        De quibus cupio scire quid sentias. Tum Torquatus: Prorsus, inquit, assentior; Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Quid iudicant sensus?
<br />
        Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? Bonum incolumis acies: misera caecitas. Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat? Et harum quidem rerum facilis est et expedita distinctio. Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Nunc omni virtuti vitium contrario nomine opponitur. Indicant pueri, in quibus ut in speculis natura cernitur. Facillimum id quidem est, inquam.
<br />
        Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. Nihil opus est exemplis hoc facere longius. Sit enim idem caecus, debilis. At cum de plurimis eadem dicit, tum certe de maximis. Qui autem esse poteris, nisi te amor ipse ceperit? Portenta haec esse dicit, neque ea ratione ullo modo posse vivi; Post enim Chrysippum eum non sane est disputatum.
<br />
        Philosophi autem in suis lectulis plerumque moriuntur. Si qua in iis corrigere voluit, deteriora fecit. Quasi ego id curem, quid ille aiat aut neget. Idemque diviserunt naturam hominis in animum et corpus. Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus; Cum sciret confestim esse moriendum eamque mortem ardentiore studio peteret, quam Epicurus voluptatem petendam putat. Sed quod proximum fuit non vidit. Atque ab his initiis profecti omnium virtutum et originem et progressionem persecuti sunt.
<br />
LORIPSUM;
   
    for($i=0; $i<$counter; $i++) {
        $content .= $loripsum;
    }
    return $content;
}
