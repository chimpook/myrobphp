<?php

define('ULTIMATEANSWER', '42');

ini_set('display_errors', 1);

$tables = array(
    // Основные таблицы
    'tbbls' => array(
        'name'=>'mt_belongings', 
        'linked_id'=>'bid', 
        'comment'=>'Шмотки'
    ),
    // Вспомогательные таблицы
    'tbses' => array(
        'name'=>'at_sessions', 
        'linked_id'=>'sid', 
        'comment'=>'Сессии'
    ),
    'tbset' => array(
        'name'=>'at_settings', 
        'linked_id'=>'setid', 
        'comment'=>'Настройки'
    ),
    'tbusr' => array(
        'name'=>'at_users', 
        'linked_id'=>'uid', 
        'comment'=>'Пользователи'
    ),
    // Справочники
    'dccat' => array(
        'name'=>'dc_categories',
        'linked_id'=>'catid',
        'comment'=>'Категории'
    ),
    'dctag' => array(
        'name'=>'dc_tags',
        'linked_id'=>'tid',
        'comment'=>'Тэги'
    ),
    'dcstt' => array(
        'name'=>'dc_statuses',
        'linked_id'=>'sid',
        'comment'=>'Статусы'
    )
);

//-------------------------------------------------------------------
// Параметры соединения
//-------------------------------------------------------------------

$parameters = array(
    'connection' => array(
        'server'=>'localhost',
        'user'=>'myrobPHP',
        'password'=>'DJq0myMYR',
        'database'=>'myrobPHP'
    )
);


//-------------------------------------------------------------------
// Ключевые каталоги проекта
//-------------------------------------------------------------------
// Путь к корневому каталогу проекта
define('ROOT_PATH', realpath($_SERVER['DOCUMENT_ROOT'])); 
define('ADDONS_PATH', '/addons');
define('MODULES_PATH', '/modules');
define('DOCS_PATH', '/docs');
define('ETC_PATH', '/etc');
define('IMG_PATH', '/img');
define('LIBS_PATH', '/libs');
define('THEMES_PATH', '/themes');
define('PAGES_PATH', '/pages');
define('TESTS_PATH', '/tests');

//-------------------------------------------------------------------
// Режимы работы 
//-------------------------------------------------------------------

// Блокировка доступа к каталогу для всех, кроме как с IP администратора
define('BLOCKMODE', false); 
// Включение отладочного режима 
// (как правило, панель со служебной информацией и логами)
define('DEBUGMODE', true); 

//-------------------------------------------------------------------
// Служебные параметры 
//-------------------------------------------------------------------

define('VERSION', '0.1');               // Версия ПО
define('THEME', 'smoothness');          // Тема
define('ADMIN_IP', '192.168.0.126');    // IP-адрес администратора

//-------------------------------------------------------------------
// Спец. символы
//-------------------------------------------------------------------
//define('RUBSYMBOL', '&#x20bd');
define('RUBSYMBOL', '<span class="glyphicon glyphicon-rub"></span>');

//-------------------------------------------------------------------
// Глобальные функции
//-------------------------------------------------------------------

require_once 'functions.php';

