<?php

ini_set('display_errors', 1);

require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/etc/globals.php';

useModule("page");
useModule("mosaic");

//$mosaic = new Mosaic(substr(loripsum(1), 0, 620), array('color'=>'rgb(233, 133, 33)', 'opacity'=>'random', 'effect'=>'switching'));
$mosaic = new Mosaic("Kontinent-Tau", array('color'=>'green', 'opacity'=>'random', 'shadow'=>'0 0 4px darkslategray', 'effect'=>'blinking'));
$page = new Page("Test of Mosaic module");
$page->append($mosaic->output());
echo $page->output();

