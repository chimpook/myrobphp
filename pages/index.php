<?php

/**
 * Online contract reginster
 *
 * Start point of app
 *
 * PHP version 5
 *
 * @category  East-Site
 * @package   myrobPHP
 * @author    Sergey Veselovsky <s.veselovsky@k-tau.ru>
 * @copyright 2015 East-Site
 * @license   http://www.gnu.org/licenses/gpl.html GPL
 * @version   GIT:$Id$
 * @link      https://chimpook@bitbucket.org/chimpook/myrobPHP.git
 */

ini_set('display_errors', 1);

require_once realpath($_SERVER["DOCUMENT_ROOT"]).'/etc/globals.php';

useModule("page");
useModule("login");
useModule("connection");
useModule("session");

$connection = new Connection();
$connection->open();
$link = $connection->getLink();

$login = new Login($link);

if ($login->isAccessGranted()) {
    $session = new Session($link);
    $account = $login->getAccount();
    $session->init($account, $account['acl']);
    $session->update();
    //header("Refresh: 3");
    //die("Да!");
    //header("Location: /pages/mosaic.php");
    header("Location: /pages/workspace.php");
} else {
    $page = new Page("myrobPHP");
    $page->append($login->output());
    echo $page->output();
}
