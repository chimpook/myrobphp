<?php

ini_set('display_errors', 1);

require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/etc/globals.php';

useModule("page");
useModule("blank");

$blank = new Blank("B L A N K");
$page = new Page("Test of Blank module");
$page->append($blank->output());
echo $page->output();

