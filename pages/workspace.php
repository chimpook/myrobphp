<?php

ini_set('display_errors', 1);

require_once realpath($_SERVER["DOCUMENT_ROOT"]).'/etc/globals.php';

useModule("page");
useModule("connection");
useModule("session");
useModule("panel");

$connection = new Connection();
$connection->open();
$link = $connection->getLink();
$page = new Page("myrobPHP");

$session = new Session($link);
if ( ! $session->check()) {
    header("Location: /");
}
//$session->locate();
$session->update();

$uid = $session->getField('uid');

// ---------------------------------------------
//
//               CONTROL PANEL
//
// ---------------------------------------------
$panel = new Panel($link, $uid);

$page->append($panel->output());

echo $page->output();
