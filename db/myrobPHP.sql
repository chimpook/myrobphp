-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: myrobPHP
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `at_sessions`
--

DROP TABLE IF EXISTS `at_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `at_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sid` varchar(64) NOT NULL,
  `updtime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=570 DEFAULT CHARSET=utf8 COMMENT='Сессии';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `at_sessions`
--

LOCK TABLES `at_sessions` WRITE;
/*!40000 ALTER TABLE `at_sessions` DISABLE KEYS */;
INSERT INTO `at_sessions` VALUES (567,0,'5s7ge9vqivqjjgcs8f95c7dgv4','2016-03-01 18:30:46'),(569,0,'1d760u67j7m692vkvef165a7t1','2016-03-01 18:32:14');
/*!40000 ALTER TABLE `at_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `at_settings`
--

DROP TABLE IF EXISTS `at_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `at_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Настройки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `at_settings`
--

LOCK TABLES `at_settings` WRITE;
/*!40000 ALTER TABLE `at_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `at_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `at_users`
--

DROP TABLE IF EXISTS `at_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `at_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `password` text NOT NULL,
  `acl` enum('r/o','r/w') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Пользователи';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `at_users`
--

LOCK TABLES `at_users` WRITE;
/*!40000 ALTER TABLE `at_users` DISABLE KEYS */;
INSERT INTO `at_users` VALUES (1,'nimda','qweasdzxc','r/w'),(2,'neko','qweasd','r/w');
/*!40000 ALTER TABLE `at_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dc_categories`
--

DROP TABLE IF EXISTS `dc_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dc_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COMMENT 'Наименование',
  `color` text COMMENT 'Цвет',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Категории документов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dc_categories`
--

LOCK TABLES `dc_categories` WRITE;
/*!40000 ALTER TABLE `dc_categories` DISABLE KEYS */;
INSERT INTO `dc_categories` VALUES (1,'Поставки','lime'),(2,'Техобслуживание','skyblue'),(3,'Инжиниринг','orange');
/*!40000 ALTER TABLE `dc_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dc_statuses`
--

DROP TABLE IF EXISTS `dc_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dc_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id Статуса',
  `name` text COMMENT 'Наименование статуса',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Статус';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dc_statuses`
--

LOCK TABLES `dc_statuses` WRITE;
/*!40000 ALTER TABLE `dc_statuses` DISABLE KEYS */;
INSERT INTO `dc_statuses` VALUES (1,'Исполняется'),(2,'Исполнено'),(3,'Расторгнуто');
/*!40000 ALTER TABLE `dc_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dc_tags`
--

DROP TABLE IF EXISTS `dc_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dc_tags` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dc_tags`
--

LOCK TABLES `dc_tags` WRITE;
/*!40000 ALTER TABLE `dc_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `dc_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mt_belongings`
--

DROP TABLE IF EXISTS `mt_belongings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mt_belongings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL,
  `name` text NOT NULL,
  `info` text NOT NULL,
  `image` blob NOT NULL,
  `place` text NOT NULL,
  `sid` int(11) NOT NULL,
  `plan` text NOT NULL,
  `date_in` date NOT NULL,
  `date_out` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Шмотки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mt_belongings`
--

LOCK TABLES `mt_belongings` WRITE;
/*!40000 ALTER TABLE `mt_belongings` DISABLE KEYS */;
/*!40000 ALTER TABLE `mt_belongings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-05 21:48:28
